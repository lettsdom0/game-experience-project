﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class Done_GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI restartText;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI livesText;

    private bool dead;
    private bool gameOver;
    private bool restart;
    static public int score = 0;
    public int savedScore;
    public int savedLives;
    static public int lives = 3;

 
   

    void Start()
    {
        dead = false;
        restart = false;
        gameOver = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        UpdateLives();
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R) && lives > 0)
            {

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else if(Input.GetKeyDown(KeyCode.R) && lives <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (dead)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
            if (lives <= 0)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void subtractLives()
    {
        lives -= 1;
        UpdateLives();
    }

    void UpdateLives()
    {
        livesText.text = "lives: " + lives;
    }

    public void AddScore()
    {
        score += 10;
        savedScore += 10;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    public void Death()
    {
        if (lives > 0)
        {
            gameOverText.text = "You died!";
            dead = true;
        }
        else
        {
            gameOverText.text = "MISSION FAILED!";
        }
        
    }

    public void Continue()
    {
        savedScore = score + savedScore;
        savedLives = lives - 1;

    }
}